﻿import java.text.DecimalFormat;
import java.util.Scanner;

public class Fahrkartenautomat {
	
	private Scanner tastatur = new Scanner(System.in);
	
	private String next;
	
	public Fahrkartenautomat() {
		double rest = fahrkartenBezahlen(fahrkartenbestellungErfassen());
		
		fahrkartenAusgeben();
		
		rueckgeldAusgeben(rest);
	}
	
	public double fahrkartenbestellungErfassen() {
		System.out.print("Zu zahlender Betrag (EURO): ");
		
		// Check if user has given a '2.5' instead of a '2,5'. If found: replace it.
		next = tastatur.next();
		
		double zuZahlenderBetrag = Double.parseDouble(next.replaceAll(",", "."));
		
		System.out.println("Anzahl der Tickets: ");
		
		int ticketAnzahl = tastatur.nextInt();
		
		while(ticketAnzahl <= 0) {
			System.out.println("Es muss mindestens ein Ticket gedruckt werden!");
			
			ticketAnzahl = tastatur.nextInt();
		}
		
		System.out.println("Gedruckte Fahrscheine: " + ticketAnzahl);
		
		zuZahlenderBetrag *= ticketAnzahl;
		
		return zuZahlenderBetrag;
	}
	
	public double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0d;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			DecimalFormat decimalFormat = new DecimalFormat("#.00");
			
			System.out.println("Noch zu zahlen: " + decimalFormat.format((zuZahlenderBetrag - eingezahlterGesamtbetrag)).replace(",", ".") + " Euro");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			
			next = tastatur.next();
			
			double eingeworfeneMünze = Double.parseDouble(next.replaceAll(",", "."));
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
		
	}
	
	public void fahrkartenAusgeben() {
		System.out.println("\nFahrscheine werden ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}
	
	// eingezahlterGesamtbetrag - zuZahlenderBetrag
	public void rueckgeldAusgeben(double restGeld) {
		double rückgabebetrag = restGeld;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) {
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			
			while (rückgabebetrag >= 1.0) {
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			
			while (rückgabebetrag >= 0.5) {
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			
			while (rückgabebetrag >= 0.2) {
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			
			while (rückgabebetrag >= 0.1) {
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			
			while (rückgabebetrag >= 0.05) {
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}
	
	public static void main(String[] args) {
		new Fahrkartenautomat();
	}
}