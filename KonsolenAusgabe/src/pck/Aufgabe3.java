package pck;

public class Aufgabe3 {

	public Aufgabe3() {
		System.out.println("Fahrenheit | Celsius");
		System.out.println("--------------------");
		
		System.out.printf("%-11s", "-20");
		System.out.print("|");
		System.out.printf("%8s", "-28.8889");
		
		System.out.println();
		
		System.out.printf("%-11s", "-10");
		System.out.print("|");
		System.out.printf("%8s", "-23.3333");
		
		System.out.println();
		
		System.out.printf("%-11s", "+0");
		System.out.print("|");
		System.out.printf("%8s", "-17.7778");
		
		System.out.println();
		
		System.out.printf("%-11s", "+20");
		System.out.print("|");
		System.out.printf("%8s", "-6.6667");
		
		System.out.println();
		
		System.out.printf("%-11s", "+30");
		System.out.print("|");
		System.out.printf("%8s", "-1.1111");
		
		System.out.println();
	}

}
