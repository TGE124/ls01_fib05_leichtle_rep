package pck;

public class Fakultät {

	public Fakultät() {
		// 0

		System.out.printf("%-5s", "0!");
		System.out.printf("%-19s", "=");
		System.out.printf("%4s", "= 1");

		System.out.println();

		// 1

		System.out.printf("%-5s", "1!");
		System.out.printf("%-19s", "= 1");
		System.out.printf("%4s", "= 1");

		System.out.println();

		// 2

		System.out.printf("%-5s", "2!");
		System.out.printf("%-19s", "= 1 * 2");
		System.out.printf("%4s", "= 2");

		System.out.println();
		
		// 3
		
		System.out.printf("%-5s", "3!");
		System.out.printf("%-19s", "= 1 * 2 * 3");
		System.out.printf("%4s", "= 6");

		System.out.println();

		// 4

		System.out.printf("%-5s", "4!");
		System.out.printf("%-20s", "= 1 * 2 * 3 * 4");
		System.out.printf("%4s", "= 24");

		System.out.println();

		// 5

		System.out.printf("%-5s", "5!");
		System.out.printf("%-20s", "= 1 * 2 * 3 * 4 * 5");
		System.out.printf("%4s", "= 120");

		System.out.println();
	}

	@Deprecated
	private int calculateFakultät(int scale) {
		int output = 1;

		for (int i = 0; i < scale; i++) {
			output *= i + 1;
		}

		return output;
	}
}
